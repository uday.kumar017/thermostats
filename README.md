# ruby 2.4.1 && Rails 5.1.7

Please run "rake db:create db:migrate db:seed" in order to insert Thermostat records into mysql database.

Below are the API URLs:

1. POST http://localhost:3000/thermostats/:household_token/readings

   Header:
    "Content-Type": "application/json"
    
   Request Body:
    {"temp": 65.2,"humidity": 100.34,"battery_charge": 45.2}

2. GET http://localhost:3000/thermostats/:household_token/readings/:id

2. GET http://localhost:3000/thermostats/:household_token
