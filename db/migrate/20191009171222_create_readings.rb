class CreateReadings < ActiveRecord::Migration[5.1]
  def change
    create_table :readings do |t|
      t.references :thermostat, foreign_key: true
      t.integer :tracking_number
      t.float :temp
      t.float :humidity
      t.float :battery_charge

      t.timestamps
    end
    add_index :readings, [:thermostat_id, :tracking_number], unique: true
  end
end
