# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
10.times do |count|
	token = SecureRandom.urlsafe_base64(16)
	Thermostat.create(household_token: token, location: "0#{count} cross road, Koramangala Block,Bangalore,India" )
end