Rails.application.routes.draw do
  get 'thermostats/show'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :thermostats, only: :show do
    resources :readings, only: [:create,:show]
  end
end
