class ReadingsController < ApplicationController
    before_action :set_reading, only: [:show]

    def show
        render json: @reading
    end

    def create
    	thermostat = Thermostat.find_by_household_token(params[:thermostat_id])
    	if thermostat.present?
	        tracking_number = Reading.last.try(:tracking_number).to_i + 1	
	        reading = thermostat.readings.build(reading_params)
	        if reading.valid?
	        	reading.tracking_number= tracking_number
	            ThermostatJob.perform_async(reading) #SuckerPunch::Job
	          render json: {msg: "Readings Successfully Added",status: :success,data: {tracking_number: tracking_number}}
	        else
	          render json: {msg: "Readings unsuccessfull",status: :unprocessable_entity}
	        end
    	else
    		render json: {msg: "Please enter valid Thermostat Id",status: :unprocessable_entity}
    	end
    end

    private

    def set_reading
      @reading = Reading.find_by_tracking_number(params[:id])
    end

    def reading_params
      params.require(:reading).permit(:temp, :humidity, :battery_charge,:thermostat_id,:tracking_number)
    end    
end
