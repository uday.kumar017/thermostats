class ThermostatsController < ApplicationController
  def show
  	thermostat = Thermostat.find_by_household_token(params[:id])
	  if thermostat.present?
	  	render json: thermostat.cal_readings
	  else
	  	render json: {msg: "Invalid household_token",status: :unprocessable_entity}
	  end
  end
end
