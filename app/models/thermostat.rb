class Thermostat < ApplicationRecord
	has_many :readings,dependent: :destroy
	validates_presence_of :household_token, :location
  def cal_readings
    columns = [:temp, :humidity, :battery_charge]
    data = self.readings
    res = {temp: {},humidity: {},battery_charge: {}}
      data.present? && columns.each do |col|
        arr = data.pluck(col)
        arr = arr.compact
        res[col][:avg] = (arr.sum / arr.size.to_f).round(2)
        res[col][:min] = (arr.min).round(2)
        res[col][:max] = (arr.max).round(2)
      end
    return res
  end
end
