class Reading < ApplicationRecord
  belongs_to :thermostat
  validates_presence_of :temp, :humidity, :battery_charge
  
end
