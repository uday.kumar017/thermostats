require 'rails_helper'

RSpec.describe Thermostat, type: :model do
  it { should have_many(:readings).dependent(:destroy) }
  it { should validate_presence_of(:household_token) }
  it { should validate_presence_of(:location) }

  describe "#cal_readings" do
    let(:thermostat) { create(:thermostat) }
    describe "having several data in Reading" do
      let!(:reading1) { create(:reading, thermostat: thermostat, temp: 1.0, humidity: 2.0, battery_charge: 3.0) }
      let!(:reading2) { create(:reading, thermostat: thermostat, temp: 2.0, humidity: 4.0, battery_charge: 6.0) }
      it "calucrated" do
        answer = thermostat.cal_readings
        expect(answer[:temp][:avg]).to eq(1.5)
        expect(answer[:humidity][:avg]).to eq(3.0)
        expect(answer[:battery_charge][:avg]).to eq(4.5)

        expect(answer[:temp][:min]).to eq(1.0)
        expect(answer[:humidity][:min]).to eq(2.0)
        expect(answer[:battery_charge][:min]).to eq(3.0)

        expect(answer[:temp][:max]).to eq(2.0)
        expect(answer[:humidity][:max]).to eq(4.0)
        expect(answer[:battery_charge][:max]).to eq(6.0)
      end
    end
    describe "having no data in Reading" do
      it "return nil" do
        answer = thermostat.cal_readings
        expect(answer[:temp][:avg]).to eq(nil)
        expect(answer[:humidity][:avg]).to eq(nil)
        expect(answer[:battery_charge][:avg]).to eq(nil)

        expect(answer[:temp][:min]).to eq(nil)
        expect(answer[:humidity][:min]).to eq(nil)
        expect(answer[:battery_charge][:min]).to eq(nil)

        expect(answer[:temp][:max]).to eq(nil)
        expect(answer[:humidity][:max]).to eq(nil)
        expect(answer[:battery_charge][:max]).to eq(nil)
      end

    end
  end
end
