require 'rails_helper'

RSpec.describe 'thermostats API', type: :request do
  let!(:thermostat) { create(:thermostat) }
  let!(:reading1) { create(:reading, thermostat: thermostat, temp: 1.0, humidity: 2.0, battery_charge: 3.0) }
  let!(:reading2) { create(:reading, thermostat: thermostat, temp: 2.0, humidity: 4.0, battery_charge: 6.0) }

  let(:household_token) { thermostat.household_token }
  describe 'GET /thermostats/:id' do
    before { get "/thermostats/#{household_token}" }

    context 'when the record exists' do
      it 'returns the thermostat' do
        expect(json).not_to be_empty
        expect(json['temp']['avg']).to eq(1.5)
        expect(json['humidity']['avg']).to eq(3.0)
        expect(json['battery_charge']['avg']).to eq(4.5)

        expect(json['temp']['min']).to eq(1.0)
        expect(json['humidity']['min']).to eq(2.0)
        expect(json['battery_charge']['min']).to eq(3.0)

        expect(json['temp']['max']).to eq(2.0)
        expect(json['humidity']['max']).to eq(4.0)
        expect(json['battery_charge']['max']).to eq(6.0)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

  end
end