require 'rails_helper'

RSpec.describe 'readings API', type: :request do
  let!(:readings) { create_list(:reading, 10) }
  let(:thermostat) { create(:thermostat) }
  let(:reading) { create(:reading, thermostat: thermostat, temp: 1.0, humidity: 2.0, battery_charge: 3.0) }
  let(:thermostat_id) { thermostat.id }
  let(:reading_id) { reading.id }

    describe 'GET /thermostats/:thermostat_id/readings/:id' do
    before { get "/thermostats/#{thermostat_id}/readings/#{reading_id}" }
    context 'when the record exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end
  end

  describe 'POST /thermostats/:thermostat_id/readings' do
    let!(:thermostat){ create(:thermostat) }
    let(:thermostat_id) { thermostat.id }

    let(:valid_attributes) { { temp: 1.0, humidity: 1.2, battery_charge: 1.3 } }

  end
end